#include <iostream>
#include <string>

using namespace std;

class Person
{
private:
    string name;
    double height;
    int age;
public:
    Person() : name("null"), height(0.0), age(0)
    {}

    Person(string name, double height, int age) : name(name), height(height), age(age)
    {}
    
    // The method prints private variables;
    void Show()
    {
        cout << "Name: " << name << '\n' << "Height: " << height << '\n'  << "Age: " << age << '\n';
    }

    void setHeight(double height)
    {
        this->height = height;
    }

    double getHeight()
    {
        return height;
    }

    void setAge(int age)
    {
        this->age = age;
    }

    int getAge()
    {
        return age;
    }
};

class Vector
{
private:
    double x;
    double y;
    double z;
    double result;
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double x, double y, double z) : x(x), y(y), z(z)
    {}

    // The method prints the value x, y, z.
    void Show()
    {
        cout << "\n" << x << ' ' << y << ' ' << z;
    }

    // The method returns the length (modulus) of the vector.
    double VectorModule()
    {
        result = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        return abs(result);
    }
};

int main()
{
    Person person("Ilias", 178, 30);
    person.Show();
    Vector v(10, 6, 33);
    v.Show();
    cout << "\nVector module: " << v.VectorModule() << '\n';
    return 0;
}